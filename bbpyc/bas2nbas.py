#!/usr/bin/env python
# -*- coding: utf-8 -*-

import docopt
import fileinput
import pathlib
import re
import sys

from typing import Dict, List

from __version__ import \
    __author__, \
    __version__

self_file = pathlib.Path(__file__)
__file_name__ = self_file.name
__project__ = self_file.stem

__doc__ = f"""
Insert numbers into Basic file.

Turns::

    .loop:
    PRINT "Hello world!"
    GOTO :loop:

into::

    10\t// .loop:
    20\tPRINT "Hello world!"
    30\tGOTO 10

and prints it to stdout.


Command line description::

    Usage:
      {__file_name__} [ BAS_FILE ]

    Options:
      BAS_FILE      Basic file to open [default: /dev/stdin].
      -h -? --help  print this screen.
      --version     print the version of this script.

    {__project__}© 2021 by {__author__} is licensed under CC BY-NC 4.0
"""


anchoring_match_re = r'^\.(\w+):$'
anchored_match_re = r'.+:(\w+):.*'


def main(argv: List[str]):
    """
    Main entrypoint. Argument is usually sys.argv[1:].
    """
    args = docopt.docopt(
        doc=__doc__,
        argv=argv,
        help=True,
        version=f"{__project__} {__version__}",
    )

    bas_file = args['BAS_FILE']

    # reads from files or stdin if files is falsy
    with fileinput.input(files=bas_file) as file:
        lines = [line[:-1] for line in file]    # strip newline

    anchors = get_anchors(lines)
    lines = comment_anchors(lines)
    lines = add_line_numbers(lines)
    lines = defer_anchor(lines, anchors)

    print(*lines, sep="\n")


class AnchorDuplicationError(Exception):
    """
    Exception be to raise when a key is used more than once to define an
    anchor.

    >>> raise AnchorDuplicationError('foo', 123)
    Traceback (most recent call last):
        ...
    AnchorDuplicationError: Anchor 'foo' already defined at position: 123.
    """

    def __init__(self, anchor, position):
        super().__init__(
            f"Anchor {anchor!r} already defined at position: {position}."
        )


class AnchorNotFoundError(Exception):
    """
    Exception be to raise when an anchor key was never defined.

    >>> raise AnchorNotFoundError('foo')
    Traceback (most recent call last):
        ...
    AnchorNotFoundError: Anchor 'foo' never defined.
    """
    def __init__(self, anchor):
        super().__init__(
            f"Anchor {anchor!r} never defined."
        )


def defer_anchor(lines: List[str], anchors: Dict[str, int]) -> List[str]:
    """
    Insert anchored lines into anchorings.

    :param lines: Un-anchored lines
    :param anchors: Anchors
    :return: Anchored lines

    >>> defer_anchor(['some code', 'goto :foo:'], dict(foo=123))
    ['some code', 'goto 1230']
    >>> defer_anchor(['goto :bar:'], dict(foo=123))
    Traceback (most recent call last):
        ...
    AnchorNotFoundError: Anchor 'foo' never defined.
    """

    for idx, line in enumerate(lines):
        referred_anchor = re.match(anchored_match_re, line)

        if not referred_anchor:
            continue

        referred_anchor = referred_anchor[1]

        try:
            ref_idx = anchors[referred_anchor]
        except KeyError:
            raise AnchorNotFoundError(referred_anchor)

        lines[idx] = line.replace(f":{referred_anchor}:", f"{ref_idx*10}")

    return lines


def add_line_numbers(lines: List[str]) -> List[str]:
    """
    Add line numbers (in 10 increments) to each line in list.

    >>> add_line_numbers(['// .foo:', 'some code', '.bar:'])
    ['10\\t// .foo:', '20\\tsome code', '30\\t.bar:']
    """

    return [
        line if line.strip().lower() == 'run' else f'{num*10}\t{line}'
        for num, line in enumerate(lines, start=1)
    ]


def comment_anchors(lines: List[str]) -> List[str]:
    """
    Turns anchor lines into comments.

    :param lines: Input lines
    :return: Anchor commented lines.

    >>> comment_anchors(['.foo:', 'some code', '.bar:'])
    ['// .foo:', 'some code', '// .bar:']
    """

    for idx, line in enumerate(lines):
        if re.match(anchoring_match_re, line):
            lines[idx] = f'// {line}'

    return lines


def get_anchors(lines: List[str]) -> Dict[str, int]:
    """
    Scans the the given lines for anchors (lines are indext starting with 1).

    :param lines: Input
    :return: Anchor {name: line number} dict.

    >>> get_anchors([''])
    {}
    >>> get_anchors(['.foo:\\n'])
    {'foo': 1}
    >>> get_anchors(['.foo:', 'some code', '.bar:'])
    {'foo': 1, 'bar': 3}
    >>> get_anchors(['.foo:', 'some code', '.foo:'])
    Traceback (most recent call last):
        ...
    AnchorDuplicationError: Anchor 'foo' already defined at position: 1.
    """

    anchors = dict()

    for num, line in enumerate(lines, start=1):
        anchor = re.match(anchoring_match_re, line)

        if not anchor:
            continue

        anchor = anchor[1]  # get first match

        if anchor in anchors:
            raise AnchorDuplicationError(
                anchor=anchor,
                position=anchors[anchor],
            )

        anchors.update({anchor: num})

    return anchors


if __name__ == '__main__':  # pragma nocover
    main(argv=sys.argv[1:])
