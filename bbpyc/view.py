#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
The view module. More views than just the console view can be added here.

© 2018 Paul Schroeder
"""

import aioconsole
import async_property
import io
import sys
import tqdm


from abc import ABC, abstractmethod
from typing import Union


class View(ABC):
    @abstractmethod
    def __init__(self, model):
        pass    # pragma: no cover

    @abstractmethod
    def get_user_input(self):
        pass    # pragma: no cover

    @abstractmethod
    def show_device_info(self):
        pass    # pragma: no cover

    @abstractmethod
    def show_device_disconnected(self):
        pass    # pragma: no cover

    @abstractmethod
    def list_found_devices(self):
        pass    # pragma: no cover

    @abstractmethod
    def show_device_response(self):
        pass    # pragma: no cover

    @abstractmethod
    def upload_progress_init(self, total_content_length: int):
        pass    # pragma: no cover

    @abstractmethod
    def upload_progress_update(self, step: int):
        pass    # pragma: no cover

    @abstractmethod
    def upload_progress_done(self) -> None:
        pass    # pragma: no cover

    def notify(self) -> None:
        """
        If the view is used as an observer it will be notified via this one.
        """

        while not self._model.changed_vars.empty():
            var = self._model.changed_vars.get()
            handle_var_func = self._var_func_lookup.get(var, None)
            handle_var_func and handle_var_func()


class ConsoleView(View):
    """
    The console view. This can be used as a template for more views.
    """

    def __init__(
        self,
        model,
        prompt: str = None,
        stdout: Union[io.TextIOWrapper, None] = None,
        stdin: Union[io.TextIOWrapper, None] = None,
    ):
        self._model = model

        self._prompt = prompt or '> '

        self._stdin = stdin or sys.stdin
        self._stdout = stdout or sys.stdout
        self._progress_bar = None

        self._model.register_observer(self)

        # vars of model to listen on and what to on change
        self._var_func_lookup = dict(
            device_info=self.show_device_info,
            devices_found=self.list_found_devices,
            current_device=self.show_device_info,
            device_response=self.show_device_response,
        )

    @async_property.async_property
    async def get_user_input(self) -> str:
        """
        Returns the input form ``stdin`` (or the given stream). Needs to be
        asnyc because it is await-ed.
        """
        return await aioconsole.ainput(
            prompt=self._prompt,
            streams=(
                self._stdin,
                self._stdout,
            )
        )

    def show_device_info(self) -> None:
        """
        Show device info supplied by the model.
        """
        for info, data in self._model.device_info.items():
            self._print(f"{info}: {data}")

    def show_device_disconnected(self) -> None:
        """
        Show the current device has been disconnected (if one was connected).
        """

        self._print(
            f"Disconnected from {self._model.current_device.address}."
            if self._model.current_device else
            "Disconnected!"
        )

    def list_found_devices(self) -> None:
        """
        List found devices (supposedly by a search).
        """
        devices = self._model.devices_found

        if not devices:
            self._print(f"No devices found!")
            return

        for dev in devices:
            self._print(f"{dev.name}: {dev.address}")

    def show_device_response(self) -> None:
        """
        Show the response of the current device after some command was sent
        to it.
        """
        self._print(self._model.device_response, end='')

    def upload_progress_init(self, total_content_length: int) -> tqdm.tqdm:
        """
        Initialize a (stepwise) upload. Could be ``pass``-ed if not wanted.

        :param total_content_length: How many bytes to be transmitted in total.
        """
        self._progress_bar = tqdm.tqdm(total=total_content_length, unit='B')
        return self._progress_bar

    def upload_progress_update(self, step: int) -> tqdm.tqdm:
        """
        An upload step to advance the a progress bar f.g. Could be ``pass``-ed
        if not wanted.

        :param step: the size of the currently completed step in bytes. (see
            ``self.upload_progress_init()`` as well.
        """
        self._progress_bar and self._progress_bar.update(step)
        return self._progress_bar

    def upload_progress_done(self) -> None:
        """
        For this view this one needs to be called on completion of an upload
        progress. Could be ``pass``-ed if not wanted.
        """
        self._progress_bar and self._progress_bar.close()

    def _print(self, *args, **kwargs) -> None:
        """
        Internal function of the view to print text to console. This could be
        implemented much different in other views and is not necessary to
        interface with the controller. It reflects pretty much the ``print()``
        signature, except that it writes to the file configures via
        ``self._stdout``.
        """
        print(*args, file=self._stdout, **kwargs)
