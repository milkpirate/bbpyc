#!/usr/bin/env python
# -*- coding: utf-8 -*-

import asyncio
import docopt
import logging
import pathlib
import sys

from typing import List

from cfg import Cfg

from model import BbpycModel
from view import ConsoleView
from controller import BbpycController

from __version__ import \
    __author__, \
    __version__, \
    __project__

__file_name__ = pathlib.Path(__file__).name

__doc__ = f"""
BlueBasic Python Console

Command line description::

    Usage:
      {__file_name__} list [ -l <log_level> ]
      {__file_name__} show -a <addr> [ -l <log_level> ]
      {__file_name__} repl -a <addr> [ -l <log_level> ]
      {__file_name__} upload -a <addr> [ -f <file> ] [ -l <log_level> ]

    Arguments:
      list      List available BlueBasic devices
      show      Show device details
      upload    Upload a file to the device
      repl      Start a REPL on the given device, can be exited via Ctrl+D
                  (Ctrl+Z <enter> on Windows).

    Options:
      -a <addr> --address=<addr>                device's bluetooth address (on Linux and
                                                  Windows its a MAC on Darwin an UUID).
      -f <file> --file=<file>                   basic file to be uploaded [default: /dev/stdin]
      -l <log_level> --log_level=<log_level>    Log level to use ({', '.join(logging._nameToLevel)})
                                                  [default: WARNING].
      -h -? --help                              print this screen.
      --version                                 print the version of this script.

    {__project__}© 2021 by {__author__} is licensed under CC BY-NC 4.0
"""


async def main(argv: List[str]):
    """
    Main function to be called on entrance.

    :param argv: Argument list (usually ``sys.argv[1:]``).
    """
    args = docopt.docopt(
        doc=__doc__,
        argv=argv,
        help=True,
        version=f"{__project__} {__version__}",
    )

    log_level = args['--log_level']
    logger = get_logger(log_level)
    logging.basicConfig(level=log_level)
    logger.debug(f"Parsed arguments: {args}")

    cfg = Cfg(args=args)
    logger.debug(f"Config: {cfg}")

    model = BbpycModel()
    logger.debug(f"Model: {model}")

    view = ConsoleView(model=model)
    logger.debug(f"View: {view}")

    async with BbpycController(
        cfg=cfg,
        model=model,
        view=view,
    ) as controller:
        logger.debug(f"Controller app: {controller}")

        try:
            await controller.run()
        except (
            KeyboardInterrupt,  # Ctrl+C
            EOFError,           # Ctrl+D (linux) / Ctrl+Z,<enter> (windows)
        ):
            exit(0)


def get_logger(level='WARNING') -> logging.Logger:
    """
    Returns a console logger set to the given level.
    """

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(level)
    stream_handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s:\t%(name)s - %(message)s'
    ))

    logger = logging.getLogger(f"{__file__}::{__name__}")
    logger.setLevel(level)
    logger.addHandler(stream_handler)

    return logger


if __name__ == '__main__':  # pragma nocover
    asyncio.run(main(argv=sys.argv[1:]))
