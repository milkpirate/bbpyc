#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Model module to hold the model and some helper classes.

© 2018 Paul Schroeder
"""

import bleak
import queue

from typing import Any, Dict, List, Set, Union


class SetAndNotify(property):
    """
    Helper decorator to ease notification of observers and keep track of
    changed variables in the model.
    """

    def __init__(self, func, doc=None) -> None:
        """
        Executed on each use/instance creation of the decorator
        """
        self.func = func
        self.__doc__ = doc or func.__doc__

    def __set__(self, obj, value) -> None:
        """
        This gets executed if ``some_obj.some_var = 123`` is issued.
        """
        obj.__dict__[self.func.__name__] = self.func(obj, value)
        obj.changed_vars.put(self.func.__name__)
        obj.notify()

    def __get__(self, obj, _) -> Any:
        """
        This gets executed if ``some_obj.some_var`` is issued.
        """
        return obj.__dict__[
            f'_{obj.__class__.__name__}__{self.func.__name__}'
        ]


class BbpycModel:
    """
    The model class for bbpyc. It holds all the information to display and
    interact with the user.
    """
    def __init__(self):
        self._observers: Set[Any] = set()

        self.changed_vars = queue.Queue(
            maxsize=6,  # number of changeable vars, see below
        )

        self.__devices_found: List[bleak.BleakClient] = []
        self.__current_device: bleak.BleakClient = None  # type: ignore
        self.__device_info: Dict[str: str] = {}
        self.__device_response: str = ''
        self.__user_input: str = ''

    def register_observer(self, observer: Any) -> None:
        """
        Registers an observer. On change, ``observer.notify()`` will be called.
        """
        self._observers.add(observer)

    def unregister_observer(self, observer: Any) -> None:
        """
        Unregister a previously registered observer.
        """
        self._observers.discard(observer)

    def notify(self) -> None:
        """
        Notifies all registered observer via their ``notify`` method.
        """
        for observer in self._observers:
            observer.notify()

    @SetAndNotify
    def device_info(self, device_info: Dict[str, str]):
        self.__device_info = device_info or {}

    @SetAndNotify
    def devices_found(
        self,
        devices_found: List[bleak.BleakClient] = None
    ) -> None:
        self.__devices_found = devices_found or []

    @SetAndNotify
    def current_device(self, device: Union[None, bleak.BleakClient]) -> None:
        self.__current_device = device

    @SetAndNotify
    def device_response(self, device_response: str) -> None:
        self.__device_response = device_response

    @SetAndNotify
    def user_input(self, user_input: str) -> None:
        self.__user_input = user_input
