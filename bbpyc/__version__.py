# -*- coding: utf-8 -*-

__all__ = [
    '__author__',
    '__version__',
    '__release__',
    '__project__'
]

__author__ = 'Paul Schroeder'
__version__ = '0.4.0'
__project__ = 'bbpyc'
__release__ = f'{__version__}.0rc'
