# -*- coding: utf-8 -*-


import io
import pathlib
import sys

from typing import Dict, Union

from cmds import Cmd
from utils import is_mac, is_uuid


class Cfg:
    """
    Configuration class to check, prepare and hold the arguments coming from
    the command line parser. It is here assumed, that only exactly one command
    is selected.
    """
    def __init__(
        self,
        args: Dict[str, Union[str, bool, None]]
    ):
        """
        Constructs the configuration for the app. Input and output streams and
        be changes at will (but must obey the io.TextIOWrapper interface).

        :param args: Commandline arguments from cmd line parser.
        """
        self.address = None
        self.file = None

        if args['list']:
            self.cmd = Cmd.listing
            return

        self.address = args['--address']
        if not is_uuid(self.address) and not is_mac(self.address):
            raise ValueError(f'{self.address!r} is not a valid UUID or MAC')

        if args['show']:
            self.cmd = Cmd.show
            return

        if args['repl']:
            self.cmd = Cmd.repl
            return

        self.file = args['--file']
        self.file = pathlib.Path(self.file)

        if args['upload']:
            self.cmd = Cmd.upload

    def __str__(self):
        return f"cmd: {self.cmd!r}\n" \
               f"address: {self.address}\n" \
               f"file: {self.file}\n"


if __name__ == "__main__":  # pragma no cover
    import doctest
    doctest.testmod()
