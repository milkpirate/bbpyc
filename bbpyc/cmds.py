# -*- coding: utf-8 -*-

from enum import Enum, auto


class Cmd(Enum):
    """
    Defines available commands implemented in app class.
    """
    listing = auto()
    show = auto()
    repl = auto()
    upload = auto()
