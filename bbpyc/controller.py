#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Controller module. Consists of just one controller, the bbpyc one.

© 2018 Paul Schroeder
"""

import asyncio
import pathlib

import bleak
import copy
import collections

from cfg import Cfg
from utils import chunkenize

from typing import Any, Union

uuid16_dict = dict((v, k) for k, v in bleak.uuids.uuid16_dict.items())


class BbpycController:
    _max_upload_chunk_size = 19  # bytes
    _bluebasic_name_prefix = "BASIC#"

    _eol = '\n'

    _cout = "c3fbc9e2-676b-9fb5-3749-2f471dcf07b2"
    _cin = "d6af9b3c-fe92-1cb2-f74b-7afb7de57e6d"

    _return_msgs = [
        "OK\n",
        "Error\n",
        "Bad expression\n",
    ]

    def __init__(
        self,
        cfg: Cfg,
        model,
        view,
    ):
        """
        Creates a controller instance.

        :param cfg: Config (basically what to so)
        :param model: The model to be used.
        :param view: The view to be used.
        """
        self._model = model
        self._view = view

        self._cfg = cfg
        self._connected = False
        self._callback_added = False
        self._ready_for_input = False
        self._client_handle = None
        self._receive_buffer = collections.deque(maxlen=4)

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc, tb):
        await self.close_connection_handle()

    async def run(self) -> None:
        """
        Runs the configured command.
        """
        cmd_name = self._cfg.cmd.name

        try:
            cmd = getattr(self, cmd_name)
        except AttributeError:
            raise NotImplementedError(f'command {cmd_name!r} not implemented!')

        await cmd()

    async def listing(self) -> None:
        """
        Prints found BlueBasic devices (i.e. with name ``BASIC#*``).
        """
        devices = await bleak.BleakScanner.discover()
        devices = [
            dev for dev in devices
            if dev.name.startswith(self._bluebasic_name_prefix)
        ]

        self._model.devices_found = devices

    async def show(self) -> None:
        """
        Get some device details (like name or firmware revision).
        """
        await self._connect()

        info_dict = {}
        for info in \
            "Device Name", \
            "Model Number String", \
            "Firmware Revision String", \
            "Manufacturer Name String" \
        :
            uuid16 = uuid16_dict.get(info)
            uuid = f"0000{uuid16:04x}-0000-1000-8000-00805f9b34fb"
            info_read = await self._client_handle.read_gatt_char(uuid)
            info_read = info_read.decode('utf-8')
            info_dict.update({info: info_read})

        self._model.device_info = copy.deepcopy(info_dict)

    async def repl(self) -> None:
        """
        Starts a REPL on the given device.
        """
        await self._connect()
        await self._add_callback_for_cout()
        await asyncio.sleep(.5)

        try:
            while True:
                if not self._ready_for_input:
                    await asyncio.sleep(.01)
                    continue

                line = await self._view.get_user_input
                await self._repl_eval(line)
                # _repl_print() is done via callback above
        except EOFError:
            # capture Ctrl+D / Ctrl+Z,<enter>
            return

    async def upload(self, file: Union[str, pathlib.Path] = None) -> None:
        """
        Uploads a (Basic) file to the given device.
        """

        file = pathlib.Path(file) if file else self._cfg.file
        content = file.read_text()

        await self._connect()
        await self._upload_content(content)

    async def _connect(self) -> None:
        if self._connected:
            return

        self._obtain_client_handle()
        self._connected = await self._client_handle.connect()

        if self._connected:
            self._client_handle.set_disconnected_callback(
                self._disconnect_callback   # type: ignore
            )

    async def _add_callback_for_cout(self) -> None:
        if self._callback_added:
            return

        await self._client_handle.start_notify(
            self._cout,
            self._repl_print
        )
        self._callback_added = True

    def _obtain_client_handle(self) -> None:
        self._client_handle = self._client_handle \
            or bleak.BleakClient(self._cfg.address)

    def _repl_print(self, char_uuid: Any, data: bytearray) -> None:
        """
        Callback to print incoming data. ``char_uuid`` gets ignored, by this
        function, but must be accepted to be used as a callback of bleak's
        client object. If ``OK\\n`` is received, the "lock" for input will be
        released.

        :param char_uuid: Integer handle of the characteristic
        :param data: Data as a byte array received
        """

        data = data.decode('utf-8')
        self._model.device_response = data
        self._view.show_device_response()

        self._receive_buffer.append(data)
        self._ready_for_input = any(
            msg in "".join(self._receive_buffer)
            for msg in self._return_msgs
        )

    def _disconnect_callback(self, client: bleak.BleakClient) -> None:
        self._connected = False
        self._callback_added = False
        self._model.current_device = client
        self._view.show_device_disconnected()

    async def close_connection_handle(self) -> None:
        """
        Explicitly closes the internal connection handle (if it is not None).
        This should not be necessary if one uses the class within a context
        manager.
        """
        if not self._client_handle:
            return

        try:
            await self._client_handle.stop_notify(self._cout)
        except (bleak.exc.BleakError, KeyError):
            pass

        if await self._client_handle.is_connected():
            await self._client_handle.disconnect()

        # otherwise we could never connect again
        self._client_handle = None

    async def _repl_eval(self, line: str) -> None:
        await self._upload_content(line)
        await asyncio.sleep(.1)  # prevent prompt and print racing

    async def _upload_content(self, content: str) -> None:
        """
        Upload the given content to the current device.

        :param content: Content to be uploaded.

        :raises: TypeError
        """

        if not content.isascii():
            raise TypeError(f'content must be ASCII only!')

        content = f'{content}{self._eol}'
        chunks = chunkenize(content, self._max_upload_chunk_size, self._eol)

        self._view.upload_progress_init(total_content_length=len(content))

        for chunk in chunks:
            chunk_bytes = bytearray(chunk, encoding="utf8")
            await self._client_handle.write_gatt_char(self._cin, chunk_bytes)
            self._view.upload_progress_update(len(chunk))
        else:
            self._view.upload_progress_done()
