#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Utils module. Holds some utility functions as the name might already suggested.

© 2018 Paul Schroeder
"""

import re

from typing import Iterator
from uuid import UUID


def chunkenize(
    content: str,
    chunk_size: int,
    sep: str,
) -> Iterator[str]:
    """
    Turn ``data`` into chunks of a maximal size ``chunk_size``. If a charakter
    in ``sep`` is encountered, the chunk is yielded early.

    :param content: Data to be split.
    :param chunk_size: Chunk size.
    :param sep: Separator charakters.
    :return: Interator to get chunks.

    >>> for c in chunkenize('foo\\nooobaaaaaar', chunk_size=7, sep='\\n'): c
    'foo\\n'
    'ooobaaa'
    'aaar'
    >>> for c in chunkenize('foo\\noooooooooo\\rbaaaaaar', chunk_size=7, sep='\\r'): c
    'foo\\nooo'
    'ooooooo'
    '\\r'
    'baaaaaa'
    'r'
    >>> for c in chunkenize('foobar', chunk_size=4, sep='\\0'): c
    'foob'
    'ar'
    """

    lines = content.split(sep)

    def line_chunks(data):
        for chunk in re.findall(f'.{{1,{chunk_size}}}', data, re.DOTALL):
            yield chunk

    for line in lines[:-1]:
        yield from line_chunks(line+sep)

    yield from line_chunks(lines[-1])


def is_uuid(uuid_to_test: str) -> bool:
    """
    Check if the given string is a valid UUID (v4).

    :param uuid_to_test: String to be checked
    :return: ``True`` if it is a valid UUID, ``False`` otherwise.

    >>> is_uuid('c9bf9e57-1685-4c89-bafb-ff5af830be8a')
    True
    >>> is_uuid('c9bf9e58')
    False
    """

    try:
        uuid_obj = UUID(uuid_to_test, version=4)
    except ValueError:
        return False
    return str(uuid_obj) == uuid_to_test


def is_mac(mac: str) -> bool:
    """
    Check whether the given string is a MAC or not.

    :param mac: MAC to check.
    :return: Is MAC or not.

    >>> is_mac("34:ab:cd:11:22:43")
    True
    >>> is_mac("34:AB:cd:11:22:43")
    True
    >>> is_mac("34:AB:cd:11:")
    False
    >>> is_mac("no mac")
    False
    """

    matched: re.Match = re.match(
        r"^([\da-f]{2}:){5}[\da-f]{2}$",
        mac,
        flags=re.IGNORECASE,
    )
    return bool(matched)


if __name__ == "__main__":  # pragma no cover
    import doctest
    doctest.testmod()
