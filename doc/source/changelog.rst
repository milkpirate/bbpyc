.. include:: ../../CHANGELOG.rst

Recent Changes
--------------

.. git_commit_detail::
    :branch:
    :commit:

.. git_changelog::
