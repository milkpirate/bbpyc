Interfaces and Submodules
=========================

Modules
-------

.. contents:: \
  :local:
  :depth: 2

cmds
~~~~

.. automodule:: cmds

cfg
~~~

.. automodule:: cfg

model
~~~~~~~

.. automodule:: model

view
~~~~~~~

.. automodule:: view

controller
~~~~~~~~~~~~~~

.. automodule:: controller
