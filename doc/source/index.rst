.. bbpyc documentation master file, created by
   sphinx-quickstart on Tue Mar  9 13:48:19 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to bbpyc's documentation!
=================================

Documentation
=============

.. toctree::
   :maxdepth: 1

   executables
   api
   changelog

.. include:: ../../README.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
