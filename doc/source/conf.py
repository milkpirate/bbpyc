# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#

import pathlib
import sys

# -- Project information -----------------------------------------------------
repo_root = pathlib.Path('..') / '..'

for sub_path in 'bbpyc', '.':
    sub_path = repo_root / sub_path
    sub_path = sub_path.absolute()
    sub_path = str(sub_path)
    sys.path.append(sub_path)

from __version__ import *

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'm2r2',
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.intersphinx',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
    'sphinx.ext.mathjax',
    'sphinx.ext.ifconfig',
    'sphinx.ext.viewcode',
    'sphinx.ext.githubpages',
    'sphinx_git',
    'sphinx_reredirects',
]

add_function_parentheses = True
todo_include_todos = True
autodoc_member_order = 'groupwise'
autodoc_typehints = 'description'

autodoc_default_options = {
    'members': True,
    'private-members': False,       # _*
    'special-members': '__init__',  # __*
    'undoc-members': True,
    'show-inheritance': True,
    'inherited-members': True,
    'ignore-module-all': False,
    'exclude-members': None,
}


# https://gitlab.com/documatt/sphinx-reredirects/-/blob/master/docs/usage.rst
# redirects = {
#     "faq/*": "https://website.com/forum/faq",
# }


# Add any paths that contain templates here, relative to this directory.
templates_path = [
    '_templates',
]

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
# source_suffix = ['.rst', '.md']
source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'restructuredtext',
    '.md': 'markdown',
}

# The master toctree document.
master_doc = 'index'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# These patterns also affect html_static_path and html_extra_path
exclude_patterns = [
    'doc/build',
    'Thumbs.db',
    '.DS_Store',
]

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


# -- Options for HTMLHelp output ---------------------------------------------

# Output file base name for HTML help builder.
htmlhelp_basename = f'{__project__}_doc'

# -- Extension configuration -------------------------------------------------

# -- Options for intersphinx extension ---------------------------------------

# Example configuration for intersphinx: refer to the Python standard library.
intersphinx_mapping = {'https://docs.python.org/': None}

# -- Custom code  ------------------------------------------------------------


def some_manipulation_hook(app, doctree, docname):
    """
    custom code to manipulate Sphinx output can be put here.
    """
    pass


def setup(app):
    app.connect("doctree-resolved", some_manipulation_hook)
