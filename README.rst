bbpyc
=====

.. contents:: \
  :local:
  :depth: 3

What's "bbpyc"?
---------------
bbpyc stands for **B**\ lue\ **B**\ asic **Py**\ thon **C**\ onsole (pronounced
bee-bee pike).

BlueBasic devices must be programmed via a console program. There are
already some options for iOS, OSx and Linux. **bbpyc** should be platform
independent console. It is built on the excellent
`bleak <https://github.com/hbldh/bleak/>`_ python module and should run on
Windows, Darwin and Linux.

Tested on:

- Ubuntu 18.04.5
- Windows 10 (Version 1909)
- Windows 10 (Version 20H2)

Installation
------------

bbpyc and comes with some requirements, which can be installed via:

  .. code-block:: bash

    pip install -r requirements.txt
    bbpyc/bbpyc.py --help

or

  .. code-block:: bash

    poetry install --no-dev
    poetry run bbpyc/bbpyc.py --help


Usage
-----

Run the help, should be self-explanatory: :code:`./bbpyc.py --help`

References
----------

- bbpyc's `Documentation <https://milkpirate.gitlab.io/bbpyc/>`_
- `BlueBasic <https://github.com/kscheff/BlueBasic>`_ fork currently under
  development. Also includes the iOS, OSx console versions.
- `bbconsole <https://github.com/0xFACE/bbconsole>`_ C implementation for
  Linux.
- `BlueBasic-loader <https://github.com/ozarchie/BlueBasic-loader>`_
  upload BASIC files via shell script.

TODOs
-----
- The REPL still seems a little buggy, since you need to press :code:`<enter>`
  twice after input.
- Establishing the connection seems a little unstable. Could use some
  improvement.

Any PRs are most welcome!

.. figure:: https://img.shields.io/badge/made%20with-Python-1f425f.svg
  :target: https://www.python.org/
  :alt: Python logo

.. figure:: https://img.shields.io/badge/made%20with-Sphinx-1f425f.svg
  :target: https://www.sphinx-doc.org/
  :alt: Sphinx logo

.. figure:: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg
  :target: https://creativecommons.org/licenses/by-nc-sa/4.0/
  :alt: License: CC BY-NC-SA 4.0
