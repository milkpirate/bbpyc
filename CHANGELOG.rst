=========
Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_,
and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.


`Unreleased`_
-------------

Todo
~~~~
* Bump version in
  * `<bbpyc/__version__.py>`_
  * and via

  .. code-block::

    poetry version (major|minor|pathc)

* Update link list at EOF.
* Tag previous release:

  .. code-block:: bash

    version="X.Y.Z"
    git tag -a "v$version" <commit hash> -m "Release v$version"
    git push origin --tags


`0.4.0`_ - 2021-06-24
---------------------

Added
~~~~~
* Gitlab built in security / vulnerability tests
* ABC class for view (and use it with ConsoleView)

Changed
~~~~~~~
* Make controller a context manager.
* Restructure internals of controller.
* Let model tell which var changed last.
* Let view react to that.

Removed
~~~~~~~
* Unused files


`0.3.0`_ - 2021-06-24
---------------------

Changed
~~~~~~~
* Total overhaul of the internal architecture (implement MVC).
* Updated documentation.


`0.2.0`_ - 2021-06-09
---------------------

Changed
~~~~~~~
* Total overhaul of the internal architecture.
* Minor changes.
* Updated documentation.

Added
~~~~~

* `<bbpyc/bas2nbas.py>`_ to add numbers and insert anchor
  references to Basic files.


`0.1.0`_ - 2021-03-09
---------------------

Added
~~~~~

* Project creation

.. _Unreleased: https://gitlab.com/milkpirate/bbpyc/compare/v0.4.0...master
.. _0.4.0: https://gitlab.com/milkpirate/bbpyc/compare/v0.3.0...v0.4.0
.. _0.3.0: https://gitlab.com/milkpirate/bbpyc/compare/v0.2.0...v0.3.0
.. _0.2.0: https://gitlab.com/milkpirate/bbpyc/compare/v0.1.0...v0.2.0
.. _0.1.0: https://gitlab.com/milkpirate/bbpyc/tag/v0.1.0
