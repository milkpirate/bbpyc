#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import asyncio
import tempfile
import time

import bleak
import pathlib
import threading

import mock
import pytest

from types import SimpleNamespace


from bbpyc.cfg import Cfg
from bbpyc.cmds import Cmd

from bbpyc.controller import BbpycController
from bbpyc.model import BbpycModel
from bbpyc.view import ConsoleView


@pytest.fixture
def controller():
    cfg = Cfg({'list': True})
    model = BbpycModel()
    view = ConsoleView(model=model)
    return BbpycController(
        cfg=cfg,
        model=model,
        view=view,
    )


def test_constructor():
    cfg = Cfg({'list': True})
    controller = BbpycController(
        cfg=cfg,
        model=object(),
        view=object(),
    )

    assert controller._cfg == cfg
    assert controller._ready_for_input is False
    assert controller._connected is False
    assert controller._callback_added is False
    assert controller._client_handle is None
    assert list(controller._receive_buffer) == []


@pytest.mark.asyncio
async def test_upload(controller):
    controller._obtain_client_handle = mock.Mock()
    controller._client_handle = SimpleNamespace(connect=mock.AsyncMock())
    controller._upload_content = mock.AsyncMock()
    controller._cfg.file = pathlib.Path(tempfile.TemporaryFile().name)
    controller._cfg.file.write_text('foo')

    await controller.upload()

    controller._obtain_client_handle.assert_called_once_with()
    controller._client_handle.connect.assert_awaited_once_with()
    controller._upload_content.assert_awaited_once_with('foo')


@pytest.mark.asyncio
async def test__upload_content_raises(controller):
    controller._client_handle = SimpleNamespace(write_gatt_char=mock.AsyncMock())

    with pytest.raises(TypeError, match="content must be ASCII only!") as exp:
        await controller._upload_content("äöüß")

    controller._client_handle.write_gatt_char.assert_not_called()


@pytest.mark.asyncio
async def test__upload_content(controller):
    cin_uuid = 'd6af9b3c-fe92-1cb2-f74b-7afb7de57e6d'

    controller._view.upload_progress_init = mock.Mock()
    controller._view.upload_progress_update = mock.Mock()
    controller._view.upload_progress_done = mock.Mock()
    controller._client_handle = SimpleNamespace(write_gatt_char=mock.AsyncMock())

    await controller._upload_content(content="content"*10)

    controller._view.upload_progress_init.assert_called_once_with(
        total_content_length=71
    )
    assert controller._view.upload_progress_update.call_args_list == [
       ((19,),), ((19,),), ((19,),), ((14,),),
    ]
    assert controller._client_handle.write_gatt_char.call_args_list == [
        ((cin_uuid, bytearray(b'contentcontentconte')),),
        ((cin_uuid, bytearray(b'ntcontentcontentcon')),),
        ((cin_uuid, bytearray(b'tentcontentcontentc')),),
        ((cin_uuid, bytearray(b'ontentcontent\n')),),
    ]
    controller._view.upload_progress_done.assert_called_once_with()


def test_disconnect_callback(controller):
    client = SimpleNamespace(address="some mac/uuid")
    controller._view.show_device_disconnected = mock.Mock()
    controller._connected = True
    controller._callback_added = True

    controller._disconnect_callback(client=client)

    assert controller._model.current_device == client
    assert controller._connected is False
    assert controller._callback_added is False
    controller._view.show_device_disconnected.assert_called_once_with()


def test_get_connection_handle_already_inited(controller):
    client_handle = SimpleNamespace(set_disconnected_callback=mock.Mock())
    controller._client_handle = client_handle

    with mock.patch('bleak.BleakClient') as bleak_client_mock:
        controller._obtain_client_handle()
        assert controller._client_handle is client_handle
        bleak_client_mock.assert_not_called()
        controller._client_handle.set_disconnected_callback.assert_not_called()


def test__obtain_client_handle_not_obtained():
    mac = '00:01:02:03:04:05'
    controller = BbpycController(
        cfg=Cfg({
            'list': False,
            'show': True,
            '--address': mac
        }),
        model=object(),
        view=object(),
    )
    client_handle = object()

    with mock.patch('bleak.BleakClient') as bleak_client_mock:
        bleak_client_mock.return_value = client_handle

        controller._obtain_client_handle()

        assert controller._client_handle is client_handle
        bleak_client_mock.assert_called_once_with(mac)

        bleak_client_mock.reset_mock()
        controller._obtain_client_handle()

        assert controller._client_handle is client_handle
        bleak_client_mock.assert_not_called()


@pytest.mark.asyncio
async def test__close_connection_handle_already_closed(controller):
    assert await controller.close_connection_handle() is None


@pytest.mark.asyncio
@pytest.mark.parametrize("is_connected", [True, False])
@pytest.mark.parametrize("stop_exp", [
    None,
    bleak.exc.BleakError,
    KeyError,
])
async def test__close_connection_handle_raises(controller, stop_exp, is_connected):
    stop_notify_mock = mock.AsyncMock(side_effect=stop_exp)
    is_connected_mock = mock.AsyncMock(return_value=is_connected)
    disconnect_mock = mock.AsyncMock()

    controller._client_handle = SimpleNamespace(
        stop_notify=stop_notify_mock,
        is_connected=is_connected_mock,
        disconnect=disconnect_mock,
    )

    await controller.close_connection_handle()

    if is_connected:
        disconnect_mock.assert_called_once()
    stop_notify_mock.assert_called_once_with(controller._cout)
    assert controller._client_handle is None


@pytest.mark.asyncio
async def test__repl_eval(controller):
    line = "some repl line"

    with mock.patch('asyncio.sleep') as asyncio_sleep_mock:
        controller._upload_content = mock.AsyncMock()

        assert await controller._repl_eval(line) is None

        controller._upload_content.assert_called_once_with(line)
        asyncio_sleep_mock.assert_called_once_with(.1)


@pytest.mark.parametrize("payload, ready_for_input", [
    (["some payload", "some more"], False),
    (["some payload OK", "\n some\n more"], True),
    (["some payload Err", "or\n some more"], True),
    (["some payload Bad expre", "ssion\n some more"], True),
])
def test__repl_print(controller, payload, ready_for_input, capsys):
    exp_stdout, *_ = ''.join(payload).split('\n')

    assert controller._ready_for_input is False

    for data in payload:
        controller._repl_print("some char uuid", bytearray(data.encode()))

    assert controller._ready_for_input == ready_for_input
    assert controller._model.device_response == payload[-1]


@pytest.mark.asyncio
async def test_listing_finds(controller):
    controller._view._var_func_lookup['devices_found'] = mock.Mock()

    with mock.patch(
        'bleak.BleakScanner.discover',
        return_value=[
            SimpleNamespace(address="1st addr", name='BASIC#12'),
            SimpleNamespace(address="2nd addr", name='some-other-device'),
            SimpleNamespace(address="3rd addr", name='BASIC#0D'),
        ],
    ):
        await controller.listing()

    assert controller._model.devices_found == [
        SimpleNamespace(address="1st addr", name='BASIC#12'),
        SimpleNamespace(address="3rd addr", name='BASIC#0D'),
    ]

    controller._view._var_func_lookup['devices_found'].assert_called_once()


@pytest.mark.asyncio
async def test_listing_no_finds(controller):
    controller._view.list_found_devices = mock.Mock()

    with mock.patch(
        'bleak.BleakScanner.discover',
        return_value=[
            SimpleNamespace(address="1st addr", name='no-basic'),
            SimpleNamespace(address="2rd addr", name='some-other-device'),
        ],
    ):
        await controller.listing()

    assert controller._model.devices_found == []
    assert controller._view.list_found_devices.update.called_once_with()


@pytest.mark.asyncio
async def test_upload(tmpdir, controller):
    payload = "some content\n"
    tmp_file = pathlib.Path(tmpdir / 'test.bas')
    tmp_file.write_text(payload)

    controller._cfg.file = tmp_file
    controller._upload_content = mock.AsyncMock()
    controller._connect = mock.AsyncMock()

    await controller.upload()

    controller._upload_content.assert_awaited_once_with(payload)
    controller._connect.assert_awaited_once()


@pytest.mark.asyncio
async def test_show(controller):
    controller._connect = mock.AsyncMock()
    controller._client_handle = SimpleNamespace(
        read_gatt_char=mock.AsyncMock(side_effect=[
            b"some name",
            b"some model number",
            b"some fw rev",
            b"some manufacturer name",
        ]),
    )

    await controller.show()

    assert controller._client_handle.read_gatt_char.call_args_list == [
        (('00002a00-0000-1000-8000-00805f9b34fb',),),
        (('00002a24-0000-1000-8000-00805f9b34fb',),),
        (('00002a26-0000-1000-8000-00805f9b34fb',),),
        (('00002a29-0000-1000-8000-00805f9b34fb',),)
    ]

    assert controller._model.device_info == {
        'Device Name': 'some name',
        'Model Number String': 'some model number',
        'Firmware Revision String': 'some fw rev',
        'Manufacturer Name String': 'some manufacturer name',
    }


@pytest.mark.asyncio
async def test_repl(controller):
    sim_user_input = [
        "user input",
        "some repl input",
        EOFError,   # press ctrl-D
    ]

    controller._connect = mock.AsyncMock()
    controller._add_callback_for_cout = mock.AsyncMock()
    controller._repl_eval = mock.AsyncMock()

    repl_thread = threading.Thread(target=asyncio.run, args=(controller.repl(),))
    repl_thread.daemon = True

    with mock.patch('asyncio.sleep'), mock.patch(
        'aioconsole.ainput',
        side_effect=sim_user_input,
    ):
        repl_thread.start()
        time.sleep(.25)
        assert repl_thread.is_alive()

        controller._ready_for_input = True
        time.sleep(.5)

        assert not repl_thread.is_alive()
        assert controller._repl_eval.call_count == len(sim_user_input)-1
        assert controller._repl_eval.mock_calls == [
            # call's are kind of tuple in tuples in tuples in...
            ((inp,),) for inp in sim_user_input[:-1]
        ]


@pytest.mark.asyncio
@pytest.mark.parametrize("cmd", list(Cmd))
async def test_run(controller, cmd):
    controller.close_connection_handle = mock.AsyncMock()

    cmd_mock = mock.AsyncMock()
    controller._cfg.cmd = cmd
    setattr(controller, cmd.name, cmd_mock)

    await controller.run()

    cmd_mock.assert_called_once()


@pytest.mark.asyncio
async def test_run_not_impl_raise(controller):
    controller.close_connection_handle = mock.AsyncMock()
    current_cmd = controller._cfg.cmd.name

    delattr(controller.__class__, current_cmd)

    with pytest.raises(
        NotImplementedError,
        match="command 'listing' not implemented!"
    ) as exp:
        await controller.run()


@pytest.mark.asyncio
async def test__connect_already_connected(controller):
    controller._connected = True
    controller._obtain_client_handle = mock.Mock()

    await controller._connect()

    controller._obtain_client_handle.assert_not_called()


@pytest.mark.asyncio
async def test__connect_already_not_connected_then_connected(controller):
    controller._connected = False
    controller._obtain_client_handle = mock.Mock()
    controller._client_handle = SimpleNamespace(
        connect=mock.AsyncMock(return_value=True),
        set_disconnected_callback=mock.Mock(),
    )

    await controller._connect()

    controller._obtain_client_handle.assert_called_once()
    assert controller._connected is True
    controller._client_handle.set_disconnected_callback.called_once_with(
        controller._disconnect_callback
    )


@pytest.mark.asyncio
async def test__connect_already_not_connected_then_still_not_connected(controller):
    controller._connected = False
    controller._obtain_client_handle = mock.Mock()
    controller._client_handle = SimpleNamespace(
        connect=mock.AsyncMock(return_value=False),
        set_disconnected_callback=mock.Mock(),
    )

    await controller._connect()

    controller._obtain_client_handle.assert_called_once()
    assert controller._connected is False
    controller._client_handle.set_disconnected_callback.assert_not_called()


@pytest.mark.asyncio
async def test__add_callback_for_cout_already_there(controller):
    controller._callback_added = True
    controller._client_handle = SimpleNamespace(
        start_notify=mock.AsyncMock(),
    )

    await controller._add_callback_for_cout()

    assert controller._callback_added is True
    controller._client_handle.start_notify.assert_not_called()


@pytest.mark.asyncio
async def test__add_callback_for_cout_not_already_there(controller):
    controller._callback_added = False
    controller._client_handle = SimpleNamespace(
        start_notify=mock.AsyncMock(),
    )

    await controller._add_callback_for_cout()

    assert controller._callback_added is True
    controller._client_handle.start_notify.assert_called_once()


@pytest.mark.asyncio
async def test_context():
    cfg = Cfg({'list': True})

    async with BbpycController(
        cfg=cfg,
        model=object(),
        view=object(),
    ) as controller:
        controller.close_connection_handle = mock.AsyncMock()

    controller.close_connection_handle.assert_called_once()
