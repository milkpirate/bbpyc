#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import mock
import pytest
import re

from types import SimpleNamespace
from bbpyc import bbpyc


@pytest.mark.parametrize("logger_level, msg_level, exp_stderr", [
    ("INFO", "info", r"20\d{2}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3} INFO:\t.*::bbpyc.bbpyc - foo\n$"),
    ("WARN", "info", None),
])
def test_get_logger(capsys, logger_level, msg_level, exp_stderr):
    logger = bbpyc.get_logger(level=logger_level)
    getattr(logger, msg_level)('foo')
    stdout, stderr = capsys.readouterr()

    assert stdout == ''

    if exp_stderr is None:
        assert stderr == ''
    else:
        assert re.match(exp_stderr, stderr)


@pytest.mark.asyncio
async def test_main(capsys):
    with mock.patch(
        'bleak.BleakScanner.discover',
        return_value=[
            SimpleNamespace(address="1st addr", name='BASIC#12'),
            SimpleNamespace(address="2rd addr", name='some-other-device'),
            SimpleNamespace(address="3nd addr", name='BASIC#0D'),
        ],
    ) as discover_mock:
        await bbpyc.main(['list'])

    discover_mock.assert_called_once()
    discover_mock.assert_awaited()

    stdout, stderr = capsys.readouterr()
    assert not stderr
    assert stdout == "BASIC#12: 1st addr\nBASIC#0D: 3nd addr\n"


@pytest.mark.parametrize("code_exp, prog_raises, exit_code", [
    (KeyboardInterrupt, SystemExit, 0),
    (EOFError, SystemExit, 0),
    (ValueError, ValueError, None),
    (KeyError, KeyError, None),
])
@pytest.mark.asyncio
async def test_main_regular_exit(code_exp, prog_raises, exit_code):
    with mock.patch(
        'bleak.BleakScanner.discover',
        side_effect=code_exp,
    ):
        with pytest.raises(prog_raises) as raise_exp:
            await bbpyc.main(['list'])
            assert raise_exp.value.code == exit_code


# @pytest.mark.asyncio
# async def test_read(capsys):
#     print('abc', file=sys.stdout)
#     stdout, stderr = capsys.readouterr()
#
#     assert stderr == ''
#     assert stdout == ''
