#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import copy
import queue

import mock
import pytest
import collections

from bbpyc.model import BbpycModel, SetAndNotify


@pytest.fixture
def model():
    model = BbpycModel()
    return model


@pytest.fixture
def observer():
    class Observer:
        notify = mock.Mock()

    return Observer()


def test_SetAndNotify_constructor():
    func = mock.Mock()
    doc = "some doc str"

    obj = SetAndNotify(func)
    assert obj.func == func
    assert obj.__doc__.startswith('\n    Create a new `Mock` object.')

    obj = SetAndNotify(func, doc=doc)
    assert obj.func == func
    assert obj.__doc__ == doc


def test_SetAndNotify_set_get():
    class ObjWithNotify:
        notify = mock.Mock()
        changed_vars = queue.Queue()

        def __init__(self):
            self.__instance_var_setter = None

        @SetAndNotify
        def instance_var_setter(self, foo):
            self.__instance_var_setter = foo

    obj = ObjWithNotify()
    assert obj.instance_var_setter == None

    obj.instance_var_setter = 123
    assert obj.instance_var_setter == 123

    obj.notify.assert_called_once_with()


def test_constructor(model):
    assert model._observers == set()
    assert model.changed_vars.queue == collections.deque([])

    assert model.devices_found == []
    assert model.current_device is None
    assert model.device_info == dict()
    assert model.device_response == ''
    assert model.user_input == ''


def test_un_register_observer(model, observer):
    assert model.changed_vars.queue == collections.deque([])

    model.register_observer(observer=observer)
    model.register_observer(observer=observer)

    assert model._observers == {observer}

    model.unregister_observer(observer=observer)

    assert model._observers == set()


def test_notify(model, observer):
    observer2 = copy.deepcopy(observer)

    model.register_observer(observer=observer)
    model.register_observer(observer=observer2)
    model.notify()

    assert observer.notify.called_once_with()
    assert observer2.notify.called_once_with()


def test_model_device_info(model):
    assert model._BbpycModel__device_info == dict()

    model.device_info = None
    assert model.device_info == dict()

    model.device_info = dict(foo='bar')
    assert model.device_info == dict(foo='bar')


def test_devices_found(model):
    assert model._BbpycModel__devices_found == []

    model.devices_found = None
    assert model.devices_found == []

    model.devices_found = [123, 456]
    assert model.devices_found == [123, 456]


def test_current_device(model):
    assert model.current_device is None

    some_client_obj = object()
    model.current_device = some_client_obj
    assert model.current_device == some_client_obj


def test_device_response(model):
    assert model.device_response == ''
    model.device_response = 'foo'
    assert model.device_response == 'foo'


def test_user_input(model):
    assert model.user_input == ''
    model.user_input = 'foo'
    assert model.user_input == 'foo'
