#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import io
import mock
import pytest
import types
import sys

from bbpyc.view import ConsoleView
from .test_model import model


def test_console_viewer_contructor_default(model):
    model.register_observer = mock.Mock()
    view = ConsoleView(model)

    assert view._prompt == '> '
    assert view._stdin == sys.stdin
    assert view._stdout == sys.stdout
    assert view._progress_bar is None
    model.register_observer.assert_called_once_with(view)


@pytest.mark.parametrize("prompt", [None, '# ', '>>> '])
@pytest.mark.parametrize("stdout", [None, io.StringIO()])
@pytest.mark.parametrize("stdin", [None, io.StringIO()])
def test_console_viewer_contructor_setting(model, capsys, stdout, stdin, prompt):
    model.register_observer = mock.Mock()

    view = ConsoleView(
        model=model,
        prompt=prompt,
        stdout=stdout,
        stdin=stdin,
    )

    assert view._prompt == (prompt if prompt else '> ')
    assert view._stdout == (stdout if stdout else sys.stdout)
    assert view._stdin == (stdin if stdin else sys.stdin)
    assert view._progress_bar is None

    model.register_observer.assert_called_once_with(view)


@pytest.mark.asyncio
async def test_user_input(model):
    view = ConsoleView(model=model)

    with mock.patch(
        'aioconsole.ainput',
        side_effect=("foo",),
    ) as ainput_mock:
        inp = await view.get_user_input

    assert inp == "foo"

    with pytest.raises(
        ValueError,
        match=r'Cannot set @async_property. .*'
    ):
        view.get_user_input = "bar"


def test_show_device_info(model, capsys):
    view = ConsoleView(model=model)
    view._var_func_lookup['device_info'] = mock.Mock(
        wraps=view._var_func_lookup['device_info']
    )

    model.device_info = dict(
        foo=123,
        bar=456,
    )

    view._var_func_lookup['device_info'].assert_called_once()
    stdout, stderr = capsys.readouterr()
    assert stderr == ''
    assert stdout == 'foo: 123\nbar: 456\n'



def test_show_device_disconnected(model, capsys):
    view = ConsoleView(model=model)
    view.show_device_disconnected()
    stdout, stderr = capsys.readouterr()

    assert stdout == 'Disconnected!\n'
    assert stderr == ''

    model.current_device = types.SimpleNamespace(address='foo')
    view.show_device_disconnected()
    stdout, stderr = capsys.readouterr()

    assert stdout == 'Disconnected from foo.\n'
    assert stderr == ''


def test_list_found_devices(model, capsys):
    view = ConsoleView(model=model)
    view.list_found_devices()
    stdout, stderr = capsys.readouterr()

    assert stdout == 'No devices found!\n'
    assert stderr == ''

    # ========================================

    view.notify = mock.Mock()

    model.devices_found = [
        types.SimpleNamespace(name='foo', address='bar'),
        types.SimpleNamespace(name='foz', address='baz'),
    ]
    view.list_found_devices()

    stdout, stderr = capsys.readouterr()

    view.notify.assert_called_once()
    assert stdout == 'foo: bar\nfoz: baz\n'
    assert stderr == ''


def test_show_device_response(model, capsys):
    view = ConsoleView(model=model)
    view.show_device_response()
    stdout, stderr = capsys.readouterr()

    assert stdout == ''
    assert stderr == ''

    view.notify = mock.Mock()
    model.device_response = "foo"

    view.show_device_response()
    stdout, stderr = capsys.readouterr()

    view.notify.assert_called_once()
    assert stdout == 'foo'
    assert stderr == ''


def test_upload_progress_init(model):
    view = ConsoleView(model=model)

    tqdm = object()

    with mock.patch(
        'tqdm.tqdm',
        side_effect=(tqdm,)
    ) as tqdm_mock:
        progress_bar = view.upload_progress_init(total_content_length=123)
        assert progress_bar == tqdm
        tqdm_mock.assert_called_once_with(total=123, unit='B')


def test_upload_progress_update(model):
    view = ConsoleView(model=model)
    # view._progress_bar = None
    assert view.upload_progress_update(step=123) is None

    progress_bar_mock = types.SimpleNamespace(update=mock.Mock())
    view._progress_bar = progress_bar_mock
    assert view.upload_progress_update(10) == progress_bar_mock
    progress_bar_mock.update.assert_called_once_with(10)


def test_upload_progress_done(model):
    view = ConsoleView(model=model)
    view._progress_bar = types.SimpleNamespace(close=mock.Mock())
    view.upload_progress_done()
    view._progress_bar.close.assert_called_once_with()


def test__print(model):
    stdout = io.StringIO()

    view = ConsoleView(
        model=model,
        stdout=stdout,
    )

    view._print('foo', end='\t')

    assert stdout.getvalue() == 'foo\t'



    #   progress_bar = view.upload_progress_init(total_content_length=123)
    #     assert progress_bar == tqdm
    #     tqdm_mock.assert_called_once_with(total=123, unit='B')
    #
    # console_sub = ConsoleViewer(stdout=std_out)
    # console_sub.update('some msg', end='\r\n')
    # exp_msg = 'some msg\r\n'
    #
    # stdout, stderr = capsys.readouterr()
    # assert stderr == ''
    #
    # if std_out:
    #     assert std_out.getvalue() == exp_msg
    # else:
    #     assert stdout == exp_msg
