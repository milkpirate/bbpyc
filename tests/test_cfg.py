#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pathlib

import pytest

from bbpyc.cfg import Cfg
from bbpyc.cmds import Cmd


"""
For args parsing we assume docopt already handles the mutually excluding/
necessary arguments for the args dict. So only valid args dicts are used here.
"""


@pytest.mark.parametrize("file", [None, '/dev/stdin', 'some/unix/path', 'c:\\some\\win\\path'])
@pytest.mark.parametrize("address", ['00:01:02:03:04:05', '586125fb-d890-430c-89be-2bcb33425a9d'])
def test_cfg_init_upload(file, address):
    args = {
        '--address': address,
        '--file': file if file else '/dev/stdin',
        '--log_level': None,
        'list': False,
        'repl': False,
        'show': False,
        'upload': True,
    }

    cfg = Cfg(**{"args": args})

    # following does not work:
    #     assert cfg.cmd== Cmd.upload
    # E   assert <Cmd.upload: 4> == <Cmd.upload: 4>
    # E     +<Cmd.upload: 4>
    # E     -<Cmd.upload: 4>
    # how we import does seem to make a difference.
    # so we make a "deep" compare:

    assert cfg.cmd.name == Cmd.upload.name
    assert cfg.cmd.value == Cmd.upload.value

    assert not address or cfg.address == address
    assert not file or cfg.file == pathlib.Path(file)


@pytest.mark.parametrize("cmd", ['repl', 'show', 'list'])
def test_cfg_init_some_cmds(cmd):
    args = {
        '--address': "00:01:02:03:04:05",
        '--file': 'foo/bar.foz',
        '--log_level': "debug",
        'list': False,
        'repl': False,
        'show': False,
        'upload': False,
        cmd: True,
    }

    cfg = Cfg(args=args)
    cmd = 'listing' if cmd == 'list' else cmd

    assert cfg.cmd.name == Cmd[cmd].name
    assert cfg.cmd.value == Cmd[cmd].value

    assert cmd == 'listing' or cfg.address is "00:01:02:03:04:05"
    assert cfg.file is None


def test_cfg_init_raise_on_wrong_address():
    args = {
        '--address': 'no_mac',
        '--file': '/dev/stdin',
        '--log_level': None,
        'list': False,
        'repl': False,
        'show': False,
        'upload': False,
    }

    with pytest.raises(
        ValueError,
        match="'no_mac' is not a valid UUID or MAC"
    ):
        Cfg(args=args)


def test_cfg_stringify():
    args = {
        '--address': None,
        '--file': '/dev/stdin',
        '--log_level': None,
        'list': True,
        'repl': False,
        'show': False,
        'upload': False,
    }

    cfg = Cfg(args=args)
    stringified = str(cfg)

    print(stringified)

    assert "cmd: <Cmd.listing: 1>\n" in stringified
    assert "address: None\n" in stringified
    assert "file: None\n" in stringified
