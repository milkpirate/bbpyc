#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pathlib
import sys


module_path = pathlib.Path('.') / "bbpyc"
module_path = module_path.absolute().__str__()
sys.path.append(module_path)
