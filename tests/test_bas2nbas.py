#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import io
import mock
import pytest

from bbpyc import bas2nbas


@pytest.mark.parametrize("inp, anchs", [
    ("", dict()),
    ("""
.foo:
    some code
    some more
.bar:
    another anchor
    with some code
    """, dict(foo=2, bar=5)),
])
def test_get_anchors(inp, anchs):
    inp = inp.splitlines()
    assert bas2nbas.get_anchors(inp) == anchs


def test_get_anchors_raises():
    inp = """
.foo:
    some code
.foo:
    using the same anchor again
""".strip().splitlines()

    with pytest.raises(
        bas2nbas.AnchorDuplicationError,
        match="Anchor 'foo' already defined at position: 1",
    ):
        bas2nbas.get_anchors(inp)


def test_comment_anchors():
    inp = """
.foo:
    some code
.bar:
    some more
""".strip().splitlines()

    assert bas2nbas.comment_anchors(inp) == [
        '// .foo:',
        '    some code',
        '// .bar:',
        '    some more'
    ]


def test_add_line_numbers():
    inp = """
.foo:
    some code
.bar:
    run
    some more
""".strip().splitlines()

    assert bas2nbas.add_line_numbers(inp) == [
        '10\t.foo:',
        '20\t    some code',
        '30\t.bar:',
        '    run',
        '50\t    some more',
    ]


def test_defer_anchor():
    inp = """
10\t.foo:
20\t    some code
// .bar:
    loop body
30\t    goto :bar:
    some more code
    goto :foo:
    run
""".strip().splitlines()

    anchors = dict(
        foo=12,
        bar=234,
    )

    assert bas2nbas.defer_anchor(inp, anchors) == [
        '10\t.foo:',
        '20\t    some code',
        '// .bar:',
        '    loop body',
        '30\t    goto 2340',
        '    some more code',
        '    goto 120',
        '    run',
    ]


def test_defer_anchor_raises():
    inp = """
10\t.foo:
20\t    some code
// .bar:
    loop body
30\t    goto :not_defines:
    some more code
    goto :foo:
    run
""".strip().splitlines()

    anchors = dict(
        foo=12,
        bar=234,
    )

    with pytest.raises(
        bas2nbas.AnchorNotFoundError,
        match="Anchor 'not_defines' never defined.",
    ):
        assert bas2nbas.defer_anchor(inp, anchors) == [
            '10\t.foo:',
            '20\t    some code',
            '// .bar:',
            '    loop body',
            '30\t    goto 2340',
            '    some more code',
            '    goto 120',
            '    run',
        ]


def test_main(capsys):
    inp = """
.foo:
    some code
    some more
.bar:
    another anchor
    goto :bar:
run
""".lstrip()

    outp = """
10\t// .foo:
20\t    some code
30\t    some more
40\t// .bar:
50\t    another anchor
60\t    goto 40
run
""".lstrip()

    with mock.patch(
        'fileinput.input',
        mock.Mock(return_value=io.StringIO(inp))
    ):
        bas2nbas.main(argv=[])

    stdout, stderr = capsys.readouterr()
    assert stderr == ""
    assert stdout == outp


@pytest.mark.parametrize("inp, exp, msg", [
    (".foo:\n.foo:\n",      bas2nbas.AnchorDuplicationError, "Anchor 'foo' already defined at position: 1."),
    (".foo:\ngoto :bar:\n", bas2nbas.AnchorNotFoundError,    "Anchor 'bar' never defined."),
])
def test_main_raises_(capsys, inp, exp, msg):
    with mock.patch(
        'fileinput.input',
        mock.Mock(return_value=io.StringIO(inp))
    ), pytest.raises(exp, match=msg):
        bas2nbas.main(argv=[])
